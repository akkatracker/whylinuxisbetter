<?php require("../../entete.php"); ?> <?php require("../../base.php"); ?> <?php require("../../fonctions.php"); ?>

<div id="corps">

<h2>Games</h2>

<p>Most games are compatible with Windows, and nothing else. Some of 
them have Mac versions, and some of them have Linux versions (Quake 4, 
Neverwinter Nights, etc.), but most of them just run on Windows.</p>

<p>So if you spend a lot of time playing recent games, you should stick 
to Windows.  But you can still install Linux, keep Windows (see the 
install section), and use both of them, depending on your needs.</p>

<p>If you do like to play games but aren't very demanding on their
recency, note that software like Wine or Cedega can help you run
not-so-recent Windows games under Linux without any problem.</p>

</div>
</body>
</html>
